package sample;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class AsteriskGUI extends JFrame {
	private JPanel panel1;
	private JPanel panel2;
	private JButton button;
	private JComboBox combobox;
	private String Aatrox[] = {"Aatrox1","Aatrox2","Aatrox3","Aatrox4"};
	private JLabel label;
	private JTextField textfield;
	private JTextArea textarea;
	
	public void createPanel1() {
	    panel1 = new JPanel();
	    add(panel1);
	    panel1.add(textfield);
	    panel1.add(combobox);
	    panel1.add(button);
	    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    setResizable(false);
		setVisible(true);
		setBounds(550, 200, 300, 400);
	} 
	
	public void createPanel2() {
	    panel2 = new JPanel();
	    add(panel2);
	    add(textarea);
	}
	
	public void createButton(ActionListener listener) {
	    button = new JButton("Enter");
	    button.addActionListener(listener);
	}
	
	public void createCombobox() {
		combobox = new JComboBox(Aatrox);
		combobox.setPreferredSize(new Dimension(110, 20));
	}
	
	public String getCombobox() {
		return combobox.getSelectedItem(); 
	}
	
	
	public void createTextField() {
	}
	
	public int getTextField() {
		return textfield;
	}

	public void createTextArea() {
	}
	
}
